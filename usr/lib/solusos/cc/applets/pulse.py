#!/usr/bin/env python
import pygtk
pygtk.require("2.0")
import gtk
import dbus
import os
import os.path

''' Main Applet for PulseAudio fixes '''
class Applet:

   def __init__(self):

	self.ui = gtk.Builder()
	self.ui.add_from_file("/usr/share/solusos/cc/applets/pulse.ui")

	self.vbox = self.ui.get_object("hbox_content")
	self.vbox.unparent()


	self.get_widget = self.ui.get_object

	# The SolusOS Configuration Service
	bus = dbus.SystemBus()
	proxy = bus.get_object('com.solusos.config', '/com/solusos/Configuration')
	self.service = dbus.Interface(proxy, dbus_interface="com.solusos.config")

	infoString = "\
Sometimes sound on SolusOS can become distorted, resulting in\n\
crackling or glitchy audio\n\
Here you can switch between <b>interrupt</b> or <b>timer</b>\n\
based scheduling. Experiment with interrupt if you experience\n\
audio issues.\n\
Some applications, such as Google Chrome, may experience issues\n\
from this option. In this case switch back to timer based\n\n\
<b>This will restart the sound server and affect any applications\n\
that may currently be using it. Please close them first</b>\n"

	self.get_widget("label_info").set_markup(infoString)

	self.interrupt = self.get_widget("radiobutton_interrupt")
	self.interrupt.set_label("Interrupt based")
	self.timer = self.get_widget("radiobutton_timer")
	self.timer.set_label("Timer based")

	if not os.path.exists ("/etc/pulse/default.pa"):
		# No pulse audio..
		self.vbox.set_sensitive (False)
		failString = "<b>You are no longer using PulseAudio\nThis applet will have no effect on your system, and has been disabled</b>"
		self.get_widget("label_info").set_markup (failString)
		return

	if self.service.GetPulseAudioFixed():
		self.interrupt.set_active(True)
		self.pa_fixed = True
	else:
		self.timer.set_active(True)

		self.pa_fixed = False

	self.interrupt.connect("clicked", self.changeit_cb)
	#self.timer.connect("clicked", self.changeit_cb)

   def changeit_cb(self, w, data=None):
	if self.interrupt.get_active():
		if not self.pa_fixed:
			self.vbox.set_sensitive(False)
			self.service.SetPulseAudioFixed(True)
	else:
		if self.pa_fixed:
			self.vbox.set_sensitive(False)
			self.service.SetPulseAudioFixed(False)
	

	fixed = self.service.GetPulseAudioFixed()
	# Now to be sure. (In case polkit auth went a bit ballsed #
	# Finally..
	if fixed != self.pa_fixed:
		# Restart PA
		os.system("pulseaudio --kill")
	if fixed:
		self.interrupt.set_active(True)
		self.pa_fixed = True
	else:
		self.timer.set_active(True)

		self.pa_fixed = False



	self.vbox.set_sensitive(True)
   ''' Once we've been init'd we need to get our content sorted
       This function returns whatever our GTK content be for the
       notebook page '''
   def get_content(self):
	return self.vbox


