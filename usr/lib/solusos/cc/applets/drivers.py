#!/usr/bin/env python
import pygtk
pygtk.require("2.0")
import gtk
import gtk.glade
import os
import dbus
import gconf
import sys
sys.path.append ("/usr/lib/solusos/config")
import detection
from dbus.mainloop.glib import DBusGMainLoop
import threading

''' Main Applet for update settings '''
class Applet:

   def __init__(self):
	self.ui_file = "/usr/share/solusos/cc/applets/drivers.glade"
	self.wTree = gtk.glade.XML(self.ui_file, 'window1')
	self.vbox = self.wTree.get_widget("hbox_content")
	self.vbox.unparent()

	self.success = False

	self.get_widget = self.wTree.get_widget

	infoString = "Using this page you can enable the use of proprietary graphics\n\
drivers on this system. Once enabled you will notice greater\n\
performance from your Graphics Card.\n\
\n\
<b>PLEASE NOTE</b>\n\
Not all graphics cards require proprietary (non-free) drivers, and\n\
some may function better without them. Also be reminded that\n\
not all drivers from each manufacturer support every graphics\n\
card chipset."

	infoFailString = "Using this page you can enable the use of proprietary graphics\n\
drivers on this system. Once enabled you will notice greater\n\
performance from your Graphics Card.\n\
\n\
<b>No drivers were found for your machine</b>"

	self.get_widget("label_info").set_markup(infoString)

	self.progressbar = self.get_widget ("progressbar")

	# Testings..
	self.configured_driver = detection.get_configured_driver ()
	glxInfo = detection.get_glx_info ()

	# The SolusOS Configuration Service
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SystemBus()
	proxy = bus.get_object('com.solusos.config', '/com/solusos/Configuration')
	self.service = dbus.Interface(proxy, dbus_interface="com.solusos.config")

	widg = self.get_widget ("label_driver")


	self.service.connect_to_signal ("install_progress", self.install_cb)
	self.service.connect_to_signal ("remove_progress", self.remove_cb)
	self.service.connect_to_signal ("driver_install_complete", self.completion)

	if self.service.IsNvidiaSupported ():
		base = "Install"
		op = True
		if self.configured_driver == "nvidia":
			base = "Remove"
			op = False
			self.success = True
		widg.set_label ("%s NVIDIA proprietary driver" % base)
		self.get_widget ("button1").connect ("clicked", lambda x: self.install_remove_driver ("nvidia", op))
	elif self.service.IsAmdSupported ():
		base = "Install"
		op = True
		if self.configured_driver == "nvidia":
			base = "Remove"
			op = False
			self.success = True
		widg.set_label ("%s AMD proprietary driver" % base)
		self.get_widget ("button1").connect ("clicked", lambda x: self.install_remove_driver ("fglrx", op))
	else:
		# No graphical drivers. Oh wells.
		self.get_widget ("label_info").set_markup (infoFailString)
		self.get_widget ("button1").set_visible (False)

	self.get_widget ("frame2").set_visible (False)

   def install_remove_driver (self, driver_name, enable):
	frame = self.get_widget ("frame2")
	self.get_widget ("button1").set_sensitive (False)
	def run ():
		frame.set_visible (True)
		self.service.SetDriverEnabled (driver_name, enable)
	t = threading.Thread ( target=run)
	t.start ()

   ''' Once we've been init'd we need to get our content sorted
       This function returns whatever our GTK content be for the
       notebook page '''
   def get_content(self):
	return self.vbox

   def install_cb (self, trans, pct, excess=None):
	lab = ""
	if trans == "configure":
		if pct == -1:
			self.progressbar.set_text ("About to configure the driver")
		elif pct == 0:
			self.progressbar.set_text ("Driver configured ok - Please reboot")
			self.success = True
		else:
			# Oh crap. Driver failed to configure
			self.progressbar.set_text ("Failed to configure the driver")
		return
	if trans == "download":
		lab = "Downloading"
	else:
		lab = "Installing"

	if (pct > 0):
		fraction = float(pct)/float(100)
		self.progressbar.set_fraction (fraction)
	self.progressbar.set_text (lab)

   def remove_cb (self, pct, excess=None):
	if (pct > 0):
		fraction = float(pct)/float(100)
		self.progressbar.set_fraction (fraction)
	self.progressbar.set_text ("Removing")

   def completion (self):
	# Well, we done
	if self.success:
		self.progressbar.set_text ("Driver configuration complete")
	else:
		self.progressbar.set_text ("Error installing drivers")



