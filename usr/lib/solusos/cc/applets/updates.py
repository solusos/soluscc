#!/usr/bin/env python
import pygtk
pygtk.require("2.0")
import gtk
import gtk.glade
import os
import gconf

''' Main Applet for update settings '''
class Applet:

   def __init__(self):
	self.ui_file = "/usr/share/solusos/cc/applets/updates.glade"
	self.wTree = gtk.glade.XML(self.ui_file, 'window1')
	self.vbox = self.wTree.get_widget("hbox_content")
	self.vbox.unparent()


	self.get_widget = self.wTree.get_widget

	infoString = "To ensure your system is kept secure against any threat, and that\n\
you have the latest versions of applications, you will need to keep\n\
up to date with the <b>SolusOS</b> Software Repositories.\n\
\n\
Using the options below, you can decide how frequently your\n\
computer will check for updates, and whether to install them\n\
automatically or not."

	self.get_widget("label_info").set_markup(infoString)

	self.get_widget("label_update").set_label ("Check for updates:")
	self.get_widget("label_install").set_label ("Automatically install:")
	self.get_widget("label_upgrade").set_label ("Check for major upgrade:")


	# Set up our comboboxes

	updates = gtk.ListStore(str, int)
	updates.append(["Hourly", 3600])
	updates.append(["Daily", 86400])
	updates.append(["Weekly", 604800])
	updates.append(["Never", 0])
	updates_widget = self.get_widget("combobox_update")
	updates_widget.set_model(updates)
	self.init_combobox("/apps/gnome-packagekit/update-icon/frequency_get_updates", "combobox_update")

	# upgrades
	upgrades = gtk.ListStore(str, int)
	upgrades.append(["Daily", 86400])
	upgrades.append(["Weekly", 604800])
	upgrades.append(["Never", 0])
	upgrades_widget = self.get_widget("combobox_upgrade")
	upgrades_widget.set_model(upgrades)
	self.init_combobox("/apps/gnome-packagekit/update-icon/frequency_get_upgrades", "combobox_upgrade")

	# automatically install
	install = gtk.ListStore(str,str)
	install.append(["All", "all"])
	install.append(["Security", "security"])
	install.append(["Nothing", "none"])
	install_widget = self.get_widget("combobox_install")
	install_widget.set_model(install)
	self.init_combobox_2("/apps/gnome-packagekit/update-icon/auto_update", "combobox_install")

    # Set a string in gconf
   def set_string(self, key, value):
        client = gconf.client_get_default()
        client.set_string(key, value)

    # Get a string from gconf
   def get_string(self, key):
        client = gconf.client_get_default()
        return client.get_string(key)

    # Get an int from gconf
   def get_int(self, key):
        client = gconf.client_get_default()
        return client.get_int(key)

    # Set an int in gconf
   def set_int(self, key, value):
        client = gconf.client_get_default()
        client.set_int(key, value)


   ''' Bind the ComboBox to gconf and assign the action '''
   def init_combobox(self, key, name):
        widget = self.get_widget(name)
        conf = self.get_int(key)
        index = 0
        for row in widget.get_model():
            if(conf == row[1]):
                widget.set_active(index)
                break
            index = index +1
        widget.connect("changed", lambda x: self.combo_fallback(key, x))

   ''' Bind the ComboBox to gconf and assign the action '''
   def init_combobox_2(self, key, name):
        widget = self.get_widget(name)
        conf = self.get_string(key)
        index = 0
        for row in widget.get_model():
            if(conf == row[1]):
                widget.set_active(index)
                break
            index = index +1
        widget.connect("changed", lambda x: self.combo_fallback_2(key, x))

   ''' Fallback for all combo boxes '''
   def combo_fallback(self, key, widget):
        act = widget.get_active()
        value = widget.get_model()[act]
        self.set_int(key, value[1])

   ''' Fallback for all combo boxes '''
   def combo_fallback_2(self, key, widget):
        act = widget.get_active()
        value = widget.get_model()[act]
        self.set_string(key, value[1])

   ''' adds a notify system... '''
   def add_notify(self, key, widget):
        client = gconf.client_get_default()
        notify_id = client.notify_add(key, self.key_changed_callback, widget)
        widget.set_data('notify_id', notify_id)
        widget.set_data('client', client)
        widget.connect("destroy", self.destroy_callback)


   ''' destroy the associated notifications '''
   def destroy_callback (self, widget):
        client = widget.get_data ('client')
        notify_id = widget.get_data ('notify_id')

        if notify_id:
            client.notify_remove (notify_id)

   ''' Callback for gconf. update our internal values '''
   def key_changed_callback (self, client, cnxn_id, entry, widget):
        if( type(widget) == gtk.ComboBox ):
            # Sanity check, if its crap ignore it.
            if(entry.value.type == gconf.VALUE_STRING):
                if(not widget and not value):
                    return
            # the string in question :)
            value = entry.value.get_string()
            for row in widget.get_model():
                if(value == row[0]):
                    widget.set_active(index)
                    break
                index = index +1


   ''' Once we've been init'd we need to get our content sorted
       This function returns whatever our GTK content be for the
       notebook page '''
   def get_content(self):
	return self.vbox


