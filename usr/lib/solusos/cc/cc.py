#!/usr/bin/env python

import sys

try:
	import pygtk
	pygtk.require("2.0")
	import gtk

	import gmenu
	from pathbar2 import NavigationBar
	import gobject
	import os
	import time
	import imp
except Exception, ex:
	print ex
	sys.exit(-1)

class ControlCenter:

   def __init__(self):
	self.window = gtk.Window()
	self.window.set_size_request(735, 500)
	self.window.set_title("System Settings")
	self.window.set_icon_name("preferences-system")

	self.box = gtk.VBox()
	self.window.add(self.box)

	self.window.set_resizable(False)
	self.total_items = 0

	# purty toolbar
	self.toolbar = gtk.Toolbar()
	self.box.pack_start(self.toolbar, False)

	# content
	self.content = gtk.VBox()

	self.notebook = gtk.Notebook()

	self.pathbar = NavigationBar()
	self.pathbar.add_with_id("All Settings", self.navigate, 2, 0)

	box = gtk.HBox()
	box.pack_start(self.pathbar, True, 10)
	titem = gtk.ToolItem()
	titem.set_expand(True)
	titem.add(box)
	self.toolbar.add(titem)

	sep = gtk.SeparatorToolItem()
	sep.set_draw(False)
	sep.set_expand(True)
	self.toolbar.add(sep)

	# search
	self.search = gtk.Entry(30)
	self.search.connect("activate", self.go_search)

	self.search.connect("notify::text", self.go_search)
	self.search.connect("icon-press", self.search_icon_hit)
	self.search.set_icon_from_icon_name(1, "edit-clear")
	self.search.set_icon_from_icon_name(0, "edit-find")
	titem =  gtk.ToolItem()
	titem.add(self.search)
	self.toolbar.add(titem)

	self.window.connect("destroy", gtk.main_quit)

	self.mapper = dict()
	self.mapper[0] = "All Settings"
	self.mapper[1] = "Search Results"

	self.scroller = gtk.ScrolledWindow()
	self.scroller.set_policy (gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

	self.scroller.add_with_viewport(self.content)
	#self.box.pack_start(self.scroller, True)
	self.box.pack_start(self.notebook)

	self.models = dict()
	self.loadems()

	gtk.link_button_set_uri_hook(self.links_handler)

	self.statusbar = gtk.Statusbar()
	context_name = "soluscc"
	self.context_id = self.statusbar.get_context_id(context_name)
	message_id = self.statusbar.push(self.context_id, "SolusOS Control Center")
	self.box.pack_start(self.statusbar, False, False)


	# We need a plugin page.
	self.plugin_page = gtk.VBox()

	self.notebook.append_page(self.plugin_page, gtk.Label("Plugin Page"))
	self.page_number = 0


	self.notebook.set_show_tabs(False)

	self.window.set_position (gtk.WIN_POS_CENTER)
	self.window.show_all()

	#self.notebook.set_current_page(4)

   def go_search(self, entry, data=None):
	search_term = entry.get_text().lower()
	hits = 0
	current_item = 0
	if search_term.replace(" ","") == "":
		if self.notebook.get_current_page() == 1:
			## oops. return to home page
			self.notebook.set_current_page(0)
			self.pathbar.remove_id(2)
			self.statusbar.push(self.context_id, "SolusOS Control Center")
		return
	model_results = gtk.ListStore(str, gtk.gdk.Pixbuf, str, str)
	for model_row in self.models:
		section = self.mapper[model_row]
		for item in self.models[model_row]:

			current_item += 1
			percent = float(current_item) / float(self.total_items)
			#self.search.set_progress_fraction(percent)
			#while gtk.events_pending():
			#	gtk.main_iteration()
			name = item[0].lower()
			if search_term in name:
				hits += 1
				model_results.append([item[0], item[1], item[2], item[3]])
	#self.search.set_progress_fraction(0)
	if hits == 0:
		model_results = gtk.ListStore(str, gtk.gdk.Pixbuf, str, str)
		self.results_view.set_model(model_results)
		self.statusbar.push(self.context_id, "No results for \"%s\"" % search_term)
	else:
		self.results_view.set_model(model_results)
		self.statusbar.push(self.context_id, "Search found %d results for \"%s\"" % (hits, entry.get_text()))
	if self.notebook.get_current_page() != 1:
		# don't re-add pathbar entry if we're already on search results
		self.pathbar.add_with_id(self.mapper[1], self.navigate, 2, 1)

   def navigate(self, obj, passed):
	if obj is not self.pathbar:
		pass
	if passed is not None:

		if passed == self.notebook.get_current_page():
			return False
		if passed < 2:
			self.notebook.set_current_page(passed)
		elif passed == 20:
			# Plugin
			self.notebook.set_current_page(3)
		else:
			self.iconview.set_model(self.models[passed])
			self.notebook.set_current_page(2)

	self.page_number = passed

	if passed == 0:
		self.statusbar.push(self.context_id, "SolusOS Control Center")

   def search_icon_hit(self, entry, icon_pos, event, data=None):
	if event.button != 1:
		# Left click only.
		return
	if icon_pos == 1:
		# Clear!
		entry.set_text("")

	# Now handle the event
	self.go_search(entry)


   ''' Load an applet using the given name '''
   def load_applet(self, applet):
	baseDir = "/usr/lib/solusos/cc/applets"

	retApplet = None

	appletSource = os.path.join(baseDir, "%s.py" % applet)
	appletCompiled = os.path.join(baseDir, "%s.pyc" % applet)

	# Prefer compiled
	if os.path.exists(appletCompiled):
		retApplet = imp.load_compiled(applet, appletCompiled)
	elif os.path.exists(appletSource):
		retApplet = imp.load_source(applet, appletSource)

	if retApplet is not None:
		return retApplet.Applet()
	else:
		return None
		
   def selection_changed(self, widget, data=None):
	selected = widget.get_selected_items()
	if len(selected) != 1:
		# No selection
		n = self.notebook.get_current_page()
		if n == 1:
			hits = len(self.results_view.get_model())
			search_term = self.search.get_text()
			self.statusbar.push(self.context_id, "Search found %d results for \"%s\"" % (hits, search_term))
		else:
			self.statusbar.push(self.context_id, "%d items in this category" % len(self.models[self.page_number]))
		return
	path = selected[0]
	model = widget.get_model()
	tree_iter = model.get_iter(path)

	name = model[tree_iter][0]
	comment = model[tree_iter][2]
	self.statusbar.push(self.context_id, comment)

   def item_activated(self, widget, path, data=None):
	model = widget.get_model()
	itere = model.get_iter(path)
	item = model.get_value(itere,0)
	item = model[itere][3]
	if "config://" in item:

		name = model[itere][0]
		page = item.split("://")[1]

		applet = self.load_applet(page)

		if applet is None:
			print "[FATAL] Applet: \"%s\" not found!" % applet
		else:
			# Remove existing plugin.
			kids = self.plugin_page.get_children()
			if len(kids) > 0:
				for sprog in kids:
					self.plugin_page.remove(sprog)

			self.plugin_page.pack_start(applet.get_content(),False, 0, 0)
			self.pathbar.add_with_id(name, self.navigate, 3, 20)
	else:
		os.system("%s &" % item)

   def create_searcher(self):
	model = gtk.ListStore(str, gtk.gdk.Pixbuf, str, str)
	iconview = gtk.IconView(model)
	iconview.set_pixbuf_column(1)
	iconview.set_text_column(0)
	iconview.set_columns(-1)
	iconview.set_item_width(125)
	iconview.connect("selection-changed", self.selection_changed)
	iconview.connect("item-activated", self.item_activated)

	scroller = gtk.ScrolledWindow()
	scroller.set_shadow_type(gtk.SHADOW_NONE)
	scroller.add_with_viewport(iconview)
	scroller.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)

	wrapper = gtk.VBox()
	wrapper.pack_start(scroller, True, True, 0)
	self.results_view = iconview
	self.notebook.append_page(wrapper, gtk.Label("Search Results"))

   def create_iconview(self, node):
	model = gtk.ListStore(str, gtk.gdk.Pixbuf, str, str)
	model.set_sort_column_id(0, gtk.SORT_ASCENDING)
	for subnode in node.contents:
		exec_info = subnode.get_exec()
		icon = self.get_image(subnode.icon)
		if subnode.comment is None:
			continue
		if "%" in exec_info:
			exec_info = exec_info.replace("%f","").replace("%F","")

		self.total_items += 1
		model.append([subnode.name, icon, subnode.comment, exec_info])

	return model

   ''' Create the solusconfig items '''
   def create_solusconfig(self):
	model = gtk.ListStore(str, gtk.gdk.Pixbuf, str, str)
	model.set_sort_column_id(0, gtk.SORT_ASCENDING)

	# PulseAudio fix.
	model.append(["Sound Card", self.get_image("audio-card"), "Fix sound problems", "config://pulse"])

	# Updates
	model.append( ["Update Settings", self.get_image ("update-manager"), "Configure update behaviour", "config://updates"] )

	# Drivers
	model.append( ["Drivers", self.get_image ("jockey"), "Configure system drivers", "config://drivers"] )

	return model

   def get_image(self, imgname):
	icon = None
	newpath = os.path.join("/usr/share/pixmaps", imgname)
	try:
		if os.path.exists(imgname): # File path.
			icon = gtk.gdk.pixbuf_new_from_file_at_size(imgname, 48, 48)

		elif os.path.exists(newpath):
			icon = gtk.gdk.pixbuf_new_from_file_at_size(newpath, 48, 48)
		else:
			theme = gtk.icon_theme_get_default()
			icon = theme.load_icon(imgname, 48, 48)

	except Exception, e:
		print e
	return icon

   def navi_help(self, btn, passed):
	self.pathbar.add_with_id(self.mapper[passed], self.navigate, 2, passed)

	if passed != 0:
		model = self.models[passed]
		self.statusbar.push(self.context_id, "%d items in this category" % len(model))
	self.navigate(self.pathbar, passed)


   def links_handler(self, btn, data=None):
	link = btn.get_uri()
	page_number = int(link.split("//")[1])
	self.navi_help(btn, page_number)

   def build_index(self):
	''' We create the first page with links to the other categories '''

	page = gtk.Table(5,5)

	COLUMNS = 2
	current_column = 0
	current_row = 0

	current_page = 2
	for node in self.sys_tree.root.contents:
		if isinstance(node, gmenu.Directory):

			self.mapper[current_page] = node.name
			btn = gtk.Button()
			btn.connect("clicked", self.navi_help, current_page)
			img = None
			if "/" in node.icon:
				img = gtk.image_new_from_file(node.icon, 48, 0)
			else:
				img = gtk.image_new_from_icon_name(node.icon, gtk.ICON_SIZE_DIALOG)

			# create custom content
			holder = gtk.VBox()
			#holder.pack_start(img, False, 3, 3)
			lab = gtk.Label(node.name)
			model = self.models[current_page]
			lab.set_markup("<big>%s</big> - <small>%d items</small>" % (node.name, len(model)))
			#holder.pack_end(lab, False, 0, 0)
			btn.add(holder)
			#btn.set_image(img)

			box_link = gtk.HBox()
			link = gtk.LinkButton("dummy://%d" % current_page)
			link.set_tooltip_text(None)
			link.set_label("View Category")
			box_link.pack_start(img, False, 0, 0)
			box_link.pack_start(link, False, 0, 0)


			box_contents = gtk.VBox()
			box_contents.pack_start(gtk.HSeparator(), False, 0, 3)
			box_contents.pack_start(box_link, False, 0, 0)

			btn.set_relief (gtk.RELIEF_NONE)
			frame = gtk.Frame()
			frame.set_shadow_type (gtk.SHADOW_NONE)
			frame.set_label_widget(lab)
			frame.add(box_contents)
			frame.show_all()
			page.attach(frame, current_column, current_column+1, current_row, current_row+1,xpadding=4, ypadding=4)



			if current_column >= COLUMNS:
				current_column = 0
				current_row = current_row + 1
			else:
				current_column = current_column + 1

			current_page = current_page + 1

	# Start Solus config
	frame2 = self.create_solus_button(current_page)
	page.attach(frame2, current_column, current_column+1, current_row, current_row+1,xpadding=4, ypadding=4)
	# End solusconfig

	holder = gtk.VBox()
	holder.set_border_width(50)
	holder.pack_start(page, True, True, 0)
	return holder
	
   def create_solus_button(self, current_page):
	# Now add solusconfig section
	current_page = current_page + 1
	self.mapper[current_page] = "Configuration"
	self.models[current_page] = self.create_solusconfig()
	# create custom content
	holder = gtk.VBox()
	#holder.pack_start(img, False, 3, 3)
	lab = gtk.Label("")
	model = self.models[current_page]
	lab.set_markup("<big>%s</big> - <small>%d items</small>" % ("Configuration", len(model)))
	#holder.pack_end(lab, False, 0, 0)
	#btn.add(holder)
	#btn.set_image(img)

	img = gtk.image_new_from_pixbuf(self.get_image("computer"))

	box_link = gtk.HBox()
	link = gtk.LinkButton("dummy://%d" % current_page)
	link.set_tooltip_text(None)
	link.set_label("View Category")
	box_link.pack_start(img, False, 0, 0)
	box_link.pack_start(link, False, 0, 0)

	box_contents = gtk.VBox()
	box_contents.pack_start(gtk.HSeparator(), False, 0, 3)
	box_contents.pack_start(box_link, False, 0, 0)

	#btn.set_relief (gtk.RELIEF_NONE)
	frame = gtk.Frame()
	frame.set_shadow_type (gtk.SHADOW_NONE)
	frame.set_label_widget(lab)
	frame.add(box_contents)
	frame.show_all()

	return frame
   def loadems(self):
	self.sys_tree = gmenu.lookup_tree('gnomecc.menu')

	root = self.sys_tree.root

	# build page 0.

	self.create_searcher()

	system_additions = list()
	system_cat = None
	current_page = 2

	self.iconview = gtk.IconView()
	self.iconview.set_pixbuf_column(1)
	self.iconview.set_text_column(0)
	self.iconview.set_columns(-1)
	self.iconview.set_item_width(125)
	self.iconview.connect("selection-changed", self.selection_changed)
	self.iconview.connect("item-activated", self.item_activated)


	for node in root.contents:
		if isinstance(node, gmenu.Directory):

			iconview = self.create_iconview(node)
			self.models[current_page] = iconview

			current_page += 1
			if "System" == node.menu_id:
				system_cat = iconview
		else:
			# whack it into system category
			system_additions.append(node)

	# Things like Java and PulseAudio tend to not provide a subcategory, so end up as uncategorised.
	# Dat be ugly. So we automatically whack them into the system category
	if len(system_additions) > 0 and system_cat is not None:
		for subnode in system_additions:
			if subnode.comment is None:
				continue
			icon = self.get_image(subnode.icon)
			exec_info = subnode.get_exec()
			if "%" in exec_info:
				exec_info = exec_info.replace("%f","").replace("%F","")

			self.total_items += 1
			system_cat.append([subnode.name, icon, subnode.comment, exec_info])

	page = self.build_index()
	self.notebook.insert_page(page, gtk.Label("All Settings"), 0)

	# Use one iconview and switch between models
	wrapper = gtk.VBox()
	scroller = gtk.ScrolledWindow()
	scroller.set_shadow_type(gtk.SHADOW_NONE)
	scroller.add_with_viewport(self.iconview)
	scroller.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
	wrapper.pack_start(scroller, True, True, 0)

	self.notebook.append_page(wrapper, gtk.Label("Current"))
	self.iconview.set_model(self.models[2])
		
if __name__ == "__main__":
	cc = ControlCenter()
	gtk.main()

